;(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;(add-hook 'before-save-hook 'gofmt-before-save)

(show-paren-mode 1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(global-display-line-numbers-mode)
