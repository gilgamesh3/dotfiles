(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("gnu" . "https://elpa.gnu.org/packages/")))

(package-initialize)

(use-package doom-themes)

(use-package mingus
  :config
  (global-set-key (kbd "C-x C-m") 'mingus))

(use-package elfeed
  :bind (:map elfeed-search-mode-map
	      ("C-s" . elfeed-update)))

(use-package janet-mode
  :bind (:map janet-mode-map
	      ("<tab>" . janet-indent-line)))

(use-package company
  :init
  (setq company-idle-delay 0)
  :config
  (add-hook 'after-init-hook 'global-company-mode))

(use-package sly
  :init
  (setq inferior-lisp-program "/usr/bin/sbcl"))


(use-package erlang
  :bind (:map erlang-mode-map
	      ("C-j" . inferior-erlang)))
